! Copyright (C) 2023 Your name.
! See https://factorcode.org/license.txt for BSD license.
USING: accessors classes combinators kernel tools.test
wyrm.lexer ;
IN: wyrm.lexer.tests

{ CHAR: H CHAR: e CHAR: l CHAR: l CHAR: o f } [ 
    "Hello" <wyrm-lexer> { 
        [ next/f ] 
        [ next/f ] 
        [ next/f ] 
        [ next/f ] 
        [ next/f ]
        [ next/f ]
    } cleave
] unit-test

{ 5 } [ 
    "Hello" <wyrm-lexer> {
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [             ]
    } cleave pos>>
] unit-test

{ t } [ 
    "Hello" <wyrm-lexer> {
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [             ]
    } cleave finished>>
] unit-test

{ 4 1 } [ 
    "Hello" <wyrm-lexer> {
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [             ]
    } cleave [ col>> ] [ line>> ] bi 
] unit-test

{ 1 2 } [ 
    "Hel\nlo" <wyrm-lexer> {
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [ next/f drop ]
        [             ]
    } cleave [ col>> ] [ line>> ] bi 
] unit-test

{ "Hel" "lo" } [
    "Hel\nlo" <wyrm-lexer> {
        [ next-word/f ]
        [ next-word/f ]
    } cleave 
] unit-test


{ wyrm-var          } [ "@x"  >wyrm-token class-of ] unit-test
{ wyrm-type         } [ "It"  >wyrm-token class-of ] unit-test
{ wyrm-real         } [ "20"  >wyrm-token class-of ] unit-test
{ wyrm-atom         } [ "sq"  >wyrm-token class-of ] unit-test
{ wyrm-declare      } [ ":>"  >wyrm-token class-of ] unit-test
{ wyrm-fat-arrow    } [ "=>"  >wyrm-token class-of ] unit-test
{ wyrm-open-paren   } [ "("   >wyrm-token class-of ] unit-test
{ wyrm-horizon      } [ "--"  >wyrm-token class-of ] unit-test
{ wyrm-close-paren  } [ ")"   >wyrm-token class-of ] unit-test
{ wyrm-define       } [ "=="  >wyrm-token class-of ] unit-test
{ wyrm-end          } [ ";"   >wyrm-token class-of ] unit-test 
{ wyrm-open-square  } [ "["   >wyrm-token class-of ] unit-test
{ wyrm-close-square } [ "]"   >wyrm-token class-of ] unit-test
{ wyrm-comment      } [ ";;"  >wyrm-token class-of ] unit-test
