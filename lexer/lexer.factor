! Copyright (C) 2023 Your name.
! See https://factorcode.org/license.txt for BSD license.
USING: accessors combinators combinators.extras
combinators.short-circuit kernel math math.parser sequences
strings unicode ;
IN: wyrm.lexer

TUPLE: wyrm-lexer
    { col       integer initial: 1 }
    { pos        fixnum initial: 0 }
    { line      integer initial: 1 }
    { prog     sequence            }
    { finished  boolean initial: f }
;

: <wyrm-lexer> ( string -- wyrm-lexer ) 
   \ wyrm-lexer new swap >>prog ;

! Advancing the lexer 
: peek/f ( wyrm-lexer -- char/f )
    dup [ pos>> ] [ prog>> ] bi ?nth 
      dup [ over t >>finished drop ] unless nip ; 

: next-line ( wyrm-lexer -- wyrm-lexer )
    1 >>col [ 1 + ] change-line ;

: next-col ( wyrm-lexer -- wyrm-lexer )
    [ 1 + ] change-col ;

: update-loc ( wyrm-lexer -- wyrm-lexer )
    dup peek/f CHAR: \n = [ next-line ] [ next-col ] if ;

: update-pos ( wyrm-lexer -- wyrm-lexer )
    [ 1 + ] change-pos ;

: next/f ( wyrm-lexer -- char/f )
    [ peek/f ] keep over [ update-loc update-pos ] when drop ;

: skip ( wyrm-lexer -- )
    next/f drop ;

: skip-spaces ( wyrm-lexer -- wyrm-lexer )
    dup peek/f blank? [ dup skip skip-spaces ] when ;

: (next-word) ( wyrm-lexer sbuf -- string )
    over peek/f { [ blank? not ] [ ] } 1&&
        [ over next/f suffix (next-word) ]
        [ nip >string ] if ;

: next-word/f ( wyrm-lexer -- string )
    skip-spaces SBUF" " clone (next-word) [ f ] when-empty ;


! Base of all wyrm tokens
TUPLE: wyrm-token
    { begin integer } 
    { end   integer }
    { file   string } 
    { line  integer } ;


! Any symbol starting with an @
TUPLE: wyrm-var < wyrm-token
    { name string } ;

: >wyrm-var ( string -- wyrm-var )
     1 tail wyrm-var new swap >>name ;

: >wyrm-var/f ( string -- ? )
    dup "@" head? [ >wyrm-var ] [ drop f ] if ;



! Any symbol starting with [A-Z]
TUPLE: wyrm-type < wyrm-token 
    { name string } ;

: >wyrm-type ( string -- wyrm-type )
    wyrm-type new swap >>name ;

: >wyrm-type/f ( string -- ? )
    dup first LETTER? [ >wyrm-type ] [ drop f ] if ;


! Any real number (float or fixnum)
TUPLE: wyrm-real < wyrm-token
    { value real } 
    { source string } ;

: >wyrm-real ( string real -- wyrm-real )
    wyrm-real new swap >>value swap >>source ;

: >wyrm-real/f ( string -- wyrm-real/f )
    dup string>number [ >wyrm-real ] [ drop f ] if* ;

: matches-token? ( string string wyrm-token -- obj/f )
    [ = ] dip new f ? ; inline

TUPLE: wyrm-declare < wyrm-token ;
: >wyrm-declare/f ( string -- wyrm-declare/f )
    ":>" wyrm-declare matches-token? ;

TUPLE: wyrm-fat-arrow < wyrm-token ;
: >wyrm-fat-arrow/f ( string -- wyrm-fat-arrow/f )
    "=>" wyrm-fat-arrow matches-token? ;

! start of an effect
TUPLE: wyrm-open-paren < wyrm-token ;
: >wyrm-open-paren/f ( string -- wyrm-open-paren/f )
    "(" wyrm-open-paren matches-token? ;

! Seperates the input and out in an effect
TUPLE: wyrm-horizon < wyrm-token ;
: >wyrm-horizon/f ( string -- wyrm-horizon/f )
    "--" wyrm-horizon matches-token? ;

! end of an effect
TUPLE: wyrm-close-paren < wyrm-token ;
: >wyrm-close-paren/f ( string -- wyrm-close-paren/f )
    ")" wyrm-close-paren matches-token? ; 

TUPLE: wyrm-define < wyrm-token ;
: >wyrm-define/f ( string -- wyrm-define/f )
    "==" wyrm-define matches-token? ;

! semi-colon ends a statement in wyrm
TUPLE: wyrm-end < wyrm-token ;
: >wyrm-end/f ( string -- wyrm-end/f )
    ";" wyrm-end matches-token? ;

TUPLE: wyrm-open-square < wyrm-token ;
: >wyrm-open-square/f ( string -- wyrm )
    "[" wyrm-open-square matches-token? ;

TUPLE: wyrm-close-square < wyrm-token ;
: >wyrm-close-square/f ( string -- wyrm )
    "]" wyrm-close-square matches-token? ;

TUPLE: wyrm-comment < wyrm-token ;
: >wyrm-comment/f ( string -- wyrm )
    ";;" wyrm-comment matches-token? ;

! All other symbols
TUPLE: wyrm-atom < wyrm-token
    { name string } ;

: >wyrm-atom ( string -- wyrm-atom )
    wyrm-atom new swap >>name ;

! convert a string into a wyrm token
: >wyrm-token ( string -- wyrm-token )
    {  
        [ >wyrm-var/f          ] 
        [ >wyrm-type/f         ] 
        [ >wyrm-real/f         ] 
        [ >wyrm-declare/f      ] 
        [ >wyrm-fat-arrow/f    ] 
        [ >wyrm-open-paren/f   ] 
        [ >wyrm-horizon/f      ] 
        [ >wyrm-close-paren/f  ] 
        [ >wyrm-define/f       ] 
        [ >wyrm-end/f          ]  
        [ >wyrm-open-square/f  ] 
        [ >wyrm-close-square/f ] 
        [ >wyrm-comment/f      ] 
        [ >wyrm-atom          ]
    } 1|| ;

! Validation of tokens: some tokens cannot be empty
GENERIC: validate/! ( wyrm-token -- wyrm-token/* )
ERROR: bad-token token message ;

UNION: cant-be-empty ;

M: cant-be-empty validate/!
    dup name>> empty? [ 
        "Token's 'name' cannot be empty" bad-token 
    ] when ;

M: wyrm-token validate/! ;

! Length of the token in the source string
GENERIC: token-length ( wyrm-token -- fixnum )

UNION: monad-token
    wyrm-open-paren wyrm-close-paren wyrm-open-square 
    wyrm-close-square ;

UNION: diad-token
    wyrm-declare wyrm-fat-arrow wyrm-horizon wyrm-define ;

UNION: named-token
    wyrm-type wyrm-atom ;

M: monad-token token-length
    drop 1 ;

M: diad-token token-length
    drop 2 ;

M: named-token token-length
    name>> length ;

M: wyrm-real token-length
    source>> length ;

: set-end ( token wyrm-lexer -- )
    col>> >>end drop ;

: set-begin ( token wyrm-lexer -- )
    drop [ ] [ end>> ] [ token-length ] tri - >>begin drop ;

: set-line ( token wyrm-lexer -- )
    line>> >>line drop ;

! Lexing process
: set-location ( wyrm-lexer token -- wyrm-lexer token )
    over [ set-end ] [ set-begin ] [ set-line ] [ nip ] 2quad ;

: add-token ( vector wyrm-lexer string -- vector wyrm-lexer )
    >wyrm-token set-location validate/! '[ _ suffix ] dip ;

: (tokenize) ( vector wyrm-lexer -- vector wyrm-lexer )
    dup finished>> not [
        dup next-word/f [ add-token (tokenize) ] when*
    ] when ;

: tokenize ( program -- vector )
    <wyrm-lexer> V{ } clone swap (tokenize) drop ;
